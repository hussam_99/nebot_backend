<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TripController;

//Done

// Route::get('allUsers', [UserController::class, 'index']);
// Route::get('getOne/{destination}', [TripController::class, 'getTripsByDestination']);
// Route::middleware('auth:sanctum')->post('logout', [Authcontroller::class, 'logout']);


Route::post('login', [AuthController::class, 'login']);
Route::post('signup', [AuthController::class, 'signUp']);

Route::middleware('auth:sanctum')->prefix('trip')->group(function () {
    Route::post('/store', [TripController::class, 'store']);
    Route::get('getOne/{destination}', [TripController::class, 'getTripsByDestination']);
});

Route::middleware('auth:sanctum')->prefix('user')->group(function () {
    Route::get('allUsers', [UserController::class, 'index']);
    Route::post('logout', [Authcontroller::class, 'logout']);
});


// Route::middleware('auth:sanctum')->prefix('trip')->group(function () {
//     Route::post('/store', [TripController::class, 'store']);
//     Route::get('/index', [TripController::class, 'index']);
//     Route::get('/show/{id}', [TripController::class, 'show']);
//     Route::delete('/delete/{id}', [TripController::class, 'delete']);
//     Route::put('/update/{id}', [TripController::class, 'update']);
// });




// Route::post('signup', [UserController::class, 'store']);
// Route::get('all', [UserController::class, 'index']);

// Route::middleware('auth:sanctum')->post('user/changePassword', [UserController::class, 'updatePassword']);
// Route::middleware('auth:sanctum')->post('logout', [Authcontroller::class, 'logout']);
// user CRUD Resource
// Route::middleware('auth:sanctum')->resource('/user', UserController::class);

