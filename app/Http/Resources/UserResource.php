<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->user_name,
            'email' => $this->email,
            'role'=> ($this->trip_guide == 1 ? 'Guide' : 'Regular User'),
            'mobile'=>$this->mobile_phone,
            'image_url' => Storage::url('images/'.$imageName),

        ];
    }
}
