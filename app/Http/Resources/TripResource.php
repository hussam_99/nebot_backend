<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'trip_id' => $this->id,
            'trip_title' => $this->trip_title,
            'guide_id' => $this->guide_id,
            'date'=> $this->date,
            'description'=>$this->description,
            'price'=>$this->price,
            'destinations'=>$this->destinations,
          
        ];
    }
}
//             'days' => json_encode($request->days, true),
