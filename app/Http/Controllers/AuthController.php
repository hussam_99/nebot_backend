<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\http\Traits\ApiResponseTrait;

class AuthController extends Controller
{
    use ApiResponseTrait;

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // $user->tokens()->delete();
            $token = $user->createToken($request->userAgent())->plainTextToken;
            $data = [
                'user' => UserResource::make($user),
                'token' => $token,
            ];
            return $this->ApiResponse($data, 'login successfully');
        }
        return $this->ApiResponse(null, 'login failed', 401);
    }    
    
    

    
    public function signUp(Request $request)
    {
        // Validate user input
        $request->validate([
            'user_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' =>  'required|string|min:6'
            // 'password' => 'required|string|confirmed|min:8',
        ]);
    
        // Create a new user
        $user = User::create([
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'trip_guide' => $request->trip_guide,
            'mobile_phone' => $request->mobile_phone,// Hash the password
        ]);
    
        // Generate a token for the user
        $token = $user->createToken('auth-token')->plainTextToken;
    
        // Return the newly created user data along with the token
        return response()->json([
            'message' => 'User registered successfully',
            'user' => $user,
            'token' => $token,
        ]);
    }
    

    public function logout()
    {
        $user = Auth::user();
        $user->currentAccessToken()->delete();
        return $this->ApiResponse(null, 'token deleted successfully');
    }



}
