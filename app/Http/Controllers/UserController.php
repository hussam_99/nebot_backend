<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUserRequest;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    use ApiResponseTrait;

    public function index()
    {
        $data = User::all();
        // return response()->json(['message' => 'User created successfully', 'user' => $data]);

        return $this->ApiResponse(UserResource::collection($data), 'all users successfully');
   
    }

    public function store(StoreUserRequest $request)
    {
        $imageName = time().'.'.$request->image->extension();  
        $request->image->storeAs('images', $imageName);
        $data = User::create([
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'trip_guide' => $request->trip_guide,
            'mobile_phone' => $request->mobile_phone,
            'image' => $imageName,
        ]);
        $data->createToken($request->userAgent())->plainTextToken;
        return response()->json(['message' => 'User created successfully', 'user' => $data]);

        // return $this->$data;
        // return $this->ApiResponse(UserResource::make($data), 'user created successfully');
    }


    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}
