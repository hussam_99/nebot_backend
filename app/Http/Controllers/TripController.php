<?php

namespace App\Http\Controllers;

use App\Models\Trip;
use Illuminate\Http\Request;
use App\Http\Traits\ApiResponseTrait;
class TripController extends Controller
{
    use ApiResponseTrait;
    
    public function index()
    {
        $trips = Trip::all();
        return $this->apiResponse(TripResource::collection($trips)->resource, 'All Trips', 200);
    }

    public function store(Request $request)
    {
        $data = [];
        if($request->hasfile('images'))
        {
            foreach($request->file('images') as $image)
            {
                $imageName = time().$image->getClientOriginalName();
                $image->storeAs('images', $imageName);
                $data[] = $imageName;  
            }
        }
        $user = User::create([
            'trip_title' => $request->name,
            'guide_id' => $request->TrainingBatchID,
            'date' => $request->price,
            'description' => $request->currency,
            'price' => json_encode($request->days, true),
            'destinations' => $request->coach_id,
            'images' => json_encode($data)
        ]);

        $image_urls = array_map(function($imageName) {
            return Storage::url('images/'.$imageName);
        }, $data);
    
        return response()->json([
            'trip_title' => $request->name,
            'guide_id' => $request->TrainingBatchID,
            'date' => $request->price,
            'description' => $request->currency,
            'price' => json_encode($request->days, true),
            'destinations' => $request->coach_id,
            'images' => json_encode($data)
        ], 201);

    }



    public function getTripsByDestination(Request $request)
    {
        // Retrieve the JSON object column (e.g., 'destinations') from the Trip model
        $trips = Trip::whereJsonContains('destinations', $request->destination)->get();
        // $request->input('destination')
        return response()->json($trips);
    }
    



    /**
     * Display the specified resource.
     */
    public function show(Trip $trip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Trip $trip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Trip $trip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Trip $trip)
    {
        //
    }
}
