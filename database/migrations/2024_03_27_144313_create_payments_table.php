<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users', 'id')->nullOnDelete(); // User ID (foreign key)
            $table->foreignId('trip_id')->nullable()->constrained('trips', 'id')->nullOnDelete();; // Trip ID (foreign key)
            $table->decimal('trip_cost', 10, 2); // Trip cost (assuming 10 digits total, 2 decimal places)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};
